package com.twuc.webApp.domain.mazeGenerator;

import java.util.*;

/**
 * 代表迷宫道路网格中的一个路径点。
 */
public class GridCell {
    private final UUID id = UUID.randomUUID();
    private final HashSet<GridCell> links = new HashSet<>();
    private final int row;
    private final int column;
    private final HashMap<String, Object> tags = new HashMap<>();
    private GridCell north;
    private GridCell south;
    private GridCell east;
    private GridCell west;

    /**
     *
     *
     * @param row
     * @param column
     */
    public GridCell(int row, int column) {
        if (row < 0) throw new IllegalArgumentException(String.format("Row index %d out of range.", row));
        if (column < 0) throw new IllegalArgumentException(String.format("Column index %d out of range.", column));
        this.row = row;
        this.column = column;
    }

    public void setNeighbors(GridCell north, GridCell south, GridCell east, GridCell west) {
        this.north = north;
        this.south = south;
        this.east = east;
        this.west = west;
    }

    @SuppressWarnings("UnusedReturnValue")
    public GridCell link(GridCell gridCell) {
        return link(gridCell, true);
    }

    private GridCell link(GridCell gridCell, boolean bidirectional) {
        Objects.requireNonNull(gridCell);
        if (!isNeighbor(gridCell)) throw new IllegalArgumentException("Can only be linked to neighbors");

        links.add(gridCell);
        if (bidirectional) gridCell.link(this, false);
        return this;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean isNeighbor(GridCell gridCell) {
        if (gridCell == null) return false;
        return gridCell.equals(north) ||
                gridCell.equals(south) ||
                gridCell.equals(east) ||
                gridCell.equals(west);
    }

    @SuppressWarnings("UnusedReturnValue")
    public GridCell unlink(GridCell gridCell) {
        return unlink(gridCell, true);
    }

    @SuppressWarnings("WeakerAccess")
    public GridCell unlink(GridCell gridCell, boolean bidirectional) {
        Objects.requireNonNull(gridCell);
        if (!isNeighbor(gridCell)) throw new IllegalArgumentException("The cell is not a neighbor.");

        links.remove(gridCell);
        if (bidirectional) gridCell.unlink(this, false);
        return this;
    }

    public boolean isLinked(GridCell gridCell) {
        return gridCell != null && links.contains(gridCell);
    }

    @SuppressWarnings("Duplicates")
    public List<GridCell> getNeighbors()
    {
        ArrayList<GridCell> cells = new ArrayList<>();
        if (north != null) cells.add(north);
        if (south != null) cells.add(south);
        if (east != null) cells.add(east);
        if (west != null) cells.add(west);
        return cells;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GridCell gridCell = (GridCell) o;
        return id.equals(gridCell.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public GridCell getNorth() {
        return north;
    }

    public GridCell getSouth() {
        return south;
    }

    public GridCell getEast() {
        return east;
    }

    @SuppressWarnings("unused")
    public GridCell getWest() {
        return west;
    }

    @SuppressWarnings("WeakerAccess")
    public HashSet<GridCell> getLinks() {
        return links;
    }

    public HashMap<String, Object> getTags() {
        return tags;
    }
}
